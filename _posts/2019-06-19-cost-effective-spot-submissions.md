---
layout: post
title:  "The pitfalls of machine translation"
lang: en
categories: Law-Firm-SEO
tags: Law Technology
author: Rene M. Paccha
last_modified_at: 2019-06-26
published: true
---


Many business owners have turned to Google and/or Bing to obtain automatically generated translations of their existing website content. We call these automatic translations _machine translations_.
Machine translations have the benefit of being cheap. Unfortunately, they tend to generate mistakes that can be misleading for prospective clients. When google observes that your users become confused and leave your website prematurely, they might lower your relevancy rating.
This, in turn, will make it more difficult for future clients to find you.  
Fortunately, an SEO expert with fluency in Spanish can help you avoid this pitfall and the unfortunate after-effects.

Cost effectiveness is one of the primary considerations for any small law firm considering SEO.
I work to ensure cost effectiveness through something called _spot submissions_.  
A _spot submission_ is taking the glaringly obvious machine translation 'errors' and transforming them to fit the context and register.  Submitting those to google the machine translation rendered on the page has less opportunity to trip a person up.

### Why my services are cost effective




There are different ways of going about putting a website together.  
1. doing everything perfectly from the start
2. adding features to your web properties (more common)

There are major considerations in integrating translations.

A. is it global (we are not)
B. is it local? (yes we are)
C. do you need to have a spanish version of everything?


### C

We have the option to submit a sitemap that details the structure of the webpage, so why not submit a translation?.

<!-- This logic is as follows
A crawl has the ability the clue-in the crawling robots to what the context is held within that structure.   Why not do the same for translations? -->


## examples of #2 Pitfalls
Here is a translation example
![Auto (machine) translated county website]({{site.baseurl}}/https://www.dropbox.com/s/56q5iang5cxmlw0/Screenshot%202019-06-19%2009.36.01.png?dl=0)

This is the page the feature is offered on:
![county website with translation offering]({{site.baseurl}}/https://www.dropbox.com/s/j36y3xbxu4hltz5/Screenshot%202019-06-19%2009.40.18.png?dl=0)

This should be our clue:
![count website with translation offering showing over 30 language options ]({{site.baseurl}}/https://www.dropbox.com/s/387m9qnu7sd1qku/Screenshot%202019-06-19%2009.42.23.png?dl=0)

## A better examples of #2

Sometimes its easier to go with with a proper targeted translation.
![cook count il website few but solid translation options]({{site.baseurl}}/https://www.dropbox.com/s/8sv0zz1hzr2on9o/Screenshot%202019-06-19%2009.45.27.png?dl=0)

The above has few, but solid offerings.  Why is this better?

Errors.   I the first website we

To show the distinction in meaning:

"Things we have no problem with" to translate "Our facilites".
In this context search tool *facilidades* does not appear at all:

The top un-confusing words are "servicios" and "instalaciones":
https://context.reverso.net/translation/english-spanish/facilities



## Integrating Spot Submissions


www.cookcountyil.gov// has done good.  But do we need to go that far? no.
So here are cost effective *spot submissions" :

Catch the glaringly wrong machine translations, and submit _those_ to google as one would a sitemap.
## Generally dealing with a persion
